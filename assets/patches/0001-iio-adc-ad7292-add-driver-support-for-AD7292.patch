From dc1b99fc4c872551b048b732aca835f3576dae5d Mon Sep 17 00:00:00 2001
From: Marcelo Schmitt <marcelo.schmitt1@gmail.com>
Date: Sat, 22 Jun 2019 22:38:44 -0300
Subject: [PATCH 1/6] iio: adc: ad7292: add driver support for AD7292

The AD7292 contains all the functionality required for general- purpose
monitoring of analog signals and control of external devices, integrated
into a single-chip solution.

Datasheet:
Link: https://www.analog.com/media/en/technical-documentation/data-sheets/AD7292.PDF

This commit adds a skeleton driver for the AD7292.

Signed-off-by: Marcelo Schmitt <marcelo.schmitt1@gmail.com>
---
 MAINTAINERS                            |  7 ++
 arch/arm/configs/adi_bcm2709_defconfig |  1 +
 drivers/iio/adc/Kconfig                | 10 +++
 drivers/iio/adc/Makefile               |  1 +
 drivers/iio/adc/ad7292.c               | 98 ++++++++++++++++++++++++++
 5 files changed, 117 insertions(+)
 create mode 100644 drivers/iio/adc/ad7292.c

diff --git a/MAINTAINERS b/MAINTAINERS
index af0bc2276e55..e40e1bd2ca3b 100644
--- a/MAINTAINERS
+++ b/MAINTAINERS
@@ -822,6 +822,13 @@ S:	Supported
 F:	drivers/iio/adc/ad7124.c
 F:	Documentation/devicetree/bindings/iio/adc/adi,ad7124.txt
 
+ANALOG DEVICES INC AD7292 DRIVER
+M:	Marcelo Schmitt <marcelo.schmitt1@gmail.com>
+L:	linux-iio@vger.kernel.org
+W:	http://ez.analog.com/community/linux-device-drivers
+S:	Supported
+F:	drivers/iio/adc/ad7292.c
+
 ANALOG DEVICES INC AD7606 DRIVER
 M:	Stefan Popa <stefan.popa@analog.com>
 L:	linux-iio@vger.kernel.org
diff --git a/arch/arm/configs/adi_bcm2709_defconfig b/arch/arm/configs/adi_bcm2709_defconfig
index 0ad3798afcce..dc7757fd86ec 100644
--- a/arch/arm/configs/adi_bcm2709_defconfig
+++ b/arch/arm/configs/adi_bcm2709_defconfig
@@ -1211,6 +1211,7 @@ CONFIG_IIO_BUFFER_CB=y
 CONFIG_IIO_SW_DEVICE=y
 CONFIG_IIO_SW_TRIGGER=y
 CONFIG_ADXL372=y
+CONFIG_AD7292=y
 CONFIG_AD738X=y
 CONFIG_AD7768_1=y
 CONFIG_MCP320X=y
diff --git a/drivers/iio/adc/Kconfig b/drivers/iio/adc/Kconfig
index ff7777aa5d3e..4166872eb799 100644
--- a/drivers/iio/adc/Kconfig
+++ b/drivers/iio/adc/Kconfig
@@ -69,6 +69,16 @@ config AD7291
 	  To compile this driver as a module, choose M here: the
 	  module will be called ad7291.
 
+config AD7292
+	tristate "Analog Devices AD7292 ADC driver"
+	depends on SPI
+	help
+	  Say yes here to build support for Analog Devices AD7292
+	  8 Channel ADC with temperature sensor.
+
+	  To compile this driver as a module, choose M here: the
+	  module will be called ad7292.
+
 config AD7298
 	tristate "Analog Devices AD7298 ADC driver"
 	depends on SPI
diff --git a/drivers/iio/adc/Makefile b/drivers/iio/adc/Makefile
index 372a2ee15542..7b811f9c0699 100644
--- a/drivers/iio/adc/Makefile
+++ b/drivers/iio/adc/Makefile
@@ -12,6 +12,7 @@ obj-$(CONFIG_AD7124) += ad7124.o
 obj-$(CONFIG_AD7173) += ad7173.o
 obj-$(CONFIG_AD7266) += ad7266.o
 obj-$(CONFIG_AD7291) += ad7291.o
+obj-$(CONFIG_AD7292) += ad7292.o
 obj-$(CONFIG_AD7298) += ad7298.o
 obj-$(CONFIG_AD738X) += ad738x.o
 obj-$(CONFIG_AD7768) += ad7768-1.o
diff --git a/drivers/iio/adc/ad7292.c b/drivers/iio/adc/ad7292.c
new file mode 100644
index 000000000000..84c951f605ee
--- /dev/null
+++ b/drivers/iio/adc/ad7292.c
@@ -0,0 +1,98 @@
+// SPDX-License-Identifier: GPL-2.0
+/*
+ * Analog Devices AD7292 SPI ADC driver
+ *
+ * Copyright 2019 Analog Devices Inc.
+ */
+
+#include <linux/device.h>
+#include <linux/module.h>
+#include <linux/spi/spi.h>
+
+#include <linux/iio/iio.h>
+
+struct ad7292_state {
+	struct spi_device *spi;
+};
+
+static int ad7292_setup(struct ad7292_state *st)
+{
+	return 0;
+}
+
+static int ad7292_read_raw(struct iio_dev *indio_dev,
+			   const struct iio_chan_spec *chan,
+			   int *val, int *val2, long info)
+{
+	return 0;
+}
+
+static int ad7768_write_raw(struct iio_dev *indio_dev,
+			    struct iio_chan_spec const *chan,
+			    int val, int val2, long info)
+{
+	return 0;
+}
+
+static const struct iio_info ad7292_info = {
+	.read_raw = ad7292_read_raw,
+	.write_raw = &ad7768_write_raw,
+};
+
+static const struct iio_chan_spec ad7292_channels[] = {
+};
+
+static int ad7292_probe(struct spi_device *spi)
+{
+	struct ad7292_state *st;
+	struct iio_dev *indio_dev;
+	int ret;
+
+	indio_dev = devm_iio_device_alloc(&spi->dev, sizeof(*st));
+	if (!indio_dev)
+		return -ENOMEM;
+
+	st = iio_priv(indio_dev);
+	st->spi = spi;
+
+	spi_set_drvdata(spi, indio_dev);
+
+	indio_dev->dev.parent = &spi->dev;
+	indio_dev->name = spi_get_device_id(spi)->name;
+	indio_dev->modes = INDIO_DIRECT_MODE;
+	indio_dev->channels = ad7292_channels;
+	indio_dev->num_channels = ARRAY_SIZE(ad7292_channels);
+	indio_dev->info = &ad7292_info;
+
+	ret = ad7292_setup(st);
+	if (ret)
+		return ret;
+
+	return devm_iio_device_register(&spi->dev, indio_dev);
+}
+
+static const struct spi_device_id ad7292_id_table[] = {
+	{ "ad7292", 0 },
+	{}
+};
+MODULE_DEVICE_TABLE(spi, ad7292_id_table);
+
+static const struct of_device_id ad7292_of_match[] = {
+	{ .compatible = "adi,ad7292" },
+	{ },
+};
+MODULE_DEVICE_TABLE(of, ad7292_of_match);
+
+static struct spi_driver ad7292_driver = {
+	.driver = {
+		.name = "ad7292",
+		.of_match_table = ad7292_of_match,
+	},
+	.probe = ad7292_probe,
+	.id_table = ad7292_id_table,
+};
+module_spi_driver(ad7292_driver);
+
+MODULE_AUTHOR("Marcelo Schmitt <marcelo.schmitt1@gmail.com>");
+MODULE_DESCRIPTION("Analog Devices AD7292 ADC driver");
+MODULE_LICENSE("GPL");
-- 
2.20.1

