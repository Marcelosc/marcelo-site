---
layout: post
title:  "MAC0472 - Laboratório de Métodos Áges - Envio de patches para o kernel do linux"
date:   2018-12-02 17:01:11 -0300
categories: jekyll update
ref: mac0472-patches
lang: pt-br
---

Este é um miniguia de comandos usados para enviar um patch para o código fonte do kernel Linux.

Navegue até o diretório do repositório contendo o kernel do linux para o qual você fez alterações:

Por exemplo
```shell
cd ~/linux-iio/iio/
```

Neste diretório estarão subdiretórios importantes como `arch`, `drivers`, `scripts`, entre outros.

Utilize uma ferramenta para verificar o estilo do seu código e conferir se as suas alterações respeitam o [kernel coding style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).

Opção número 1) Instale o [KWorkflow](https://github.com/rodrigosiqueira/kworkflow) e utilize o seu codestyle:
```shell
kw codestyle <arquivo que você alterou>
```
Exemplo:
```shell
kw codestyle drivers/staging/iio/impedance-analyzer/ad5933.c
```

Opção número 2) Use ferramentas já presentes no kernel:
```shell
perl scripts/checkpatch.pl --terse --no-tree --color=always -strict --file <camiho do arquivo>
```
Exemplo:
```shell
perl scripts/checkpatch.pl --terse --no-tree --color=always -strict --file drivers/staging/iio/impedance-analyzer/ad5933.c
```

Tenha certeza de que as suas alterações compilam antes de enviar seu código para revisão da comunidade. Para isso use o Makefile na raiz do kernel.
```shell
make M=<diretório que contém os arquivos que você alterou> clean
make M=<diretório que contém os arquivos que você alterou>
```
Exemplo:
```shell
make M=drivers/staging/iio/impedance-analyzer clean
make M=drivers/staging/iio/impedance-analyzer
```

Também é recomendado instalar o kernel modificado em uma máquina virtual e iniciá-la (dar boot) mas isso é assunto para um outro post.

Revise as coisas que você fez com git diff.
```shell
git diff <arquivo que você alterou>
```
Exemplo:
```shell
git diff drivers/staging/iio/impedance-analyzer/ad5933.c
```

Adicione o arquivo com as alterações feitas aos arquivos marcados para commit.
```shell
git add <arquivo que você alterou>
```
Exemplo:
```shell
git add drivers/staging/iio/impedance-analyzer/ad5933.c
```

Faça um commit.
```shell
git commit -s -v
```
Escreva o título e mensagem de commit descrevendo quais alterações você fez e por que. [Este post](https://chris.beams.io/posts/git-commit/) explica como escrever boas mensagens de commit.

Obtenha a lista de revisores para os quais você deve enviar o e-mail com as suas modificações.

Opção 1)
```shell
kw m <caminho do arquivo>
```
Exemplo:
```shell
kw m drivers/staging/iio/impedance-analyzer/ad5933.c
```
Opção 2)
```sehll
perl scripts/get_maintainer.pl <caminho do arquivo>
```
Exemplo:
```shell
perl scripts/get_maintainer.pl drivers/staging/iio/impedance-analyzer/ad5933.c
```

Obtenha o hash o commit anterior ao commit que você fez
```shell
git log
```

Utilize o `git format-patch` para criar o e-mail que você mandará para a lista a lista de e-mails do subsistema em que está trabalhando e responsáveis pelos arquivos modificados. Substitua os e-mails após `--to` pelos e-mails obtidos dos mantenedores do(s) arquivos que você alterou. No final coloque o hash do commit anterior ao commit das suas alterações.
```shell
git format-patch -o <local onde salvar o patch> --to="mandenedor1@dominio.com,mantenedor2@dominio2.org,mantenedorN@outrodominio.org" --cc="listadeemail1@dominio.org,listadeemail2@dominio.org,listadeemailN@outro.org" <hash do último commit antes do seu>
```
<!--
git format-patch -o ~/ --to="lars@metafoo.de,Michael.Hennerich@analog.com,jic23@kernel.org,knaack.h@gmx.de,pmeerw@pmeerw.net" --cc="linux-iio@vger.kernel.org,linux-kernel@vger.kernel.org,kernel-usp@googlegroups.com" --no-renames 430583493627eea5bbd95c913bc1bc23b2f5a046

<renatogeh> git format-patch -o /tmp/ --to="lars@metafoo.de,Michael.Hennerich@analog.com,jic23@kernel.org,knaack.h@gmx.de,pmeerw@pmeerw.net,gregkh@linuxfoundation.org,stefan.popa@analog.com,alexandru.Ardelean@analog.com,giuliano.belinassi@usp.br" --cc="linux-iio@vger.kernel.org,devel@driverdev.osuosl.org,linux-kernel@vger.kernel.org,kernel-usp@googlegroups.com" --no-renames --cover-letter -v2 78acca

git format-patch -o ~/ --to="lars@metafoo.de,Michael.Hennerich@analog.com,jic23@kernel.org,knaack.h@gmx.de,pmeerw@pmeerw.net" --cc="linux-iio@vger.kernel.org,linux-kernel@vger.kernel.org,kernel-usp@googlegroups.com" --no-renames --cover-letter -v2 66d388ff4a73d2f930fa3c9d5ade259522769ea5

-->
Exemplo:
```shell
git format-patch -o ~/ --to="mandenedor1@dominio.com,mantenedor2@dominio2.org,mantenedorN@outrodominio.org" --cc="listadeemail1@dominio.org,listadeemail2@dominio.org,listadeemailN@outro.org" e27aea341e1739b0f784e4656ed1c57d057dd8b3
```
Navegue até o local onde você salvou o seu pacth.

Exemplo:
```shell
cd ~/
```
Você pode abrir o seu patch com um editor de texto e confirir novamente as alterações feitas e endereços de e-mail.

Exemplo:
```shell
vim ~/0001-staging-iio-ad5933-replaced-kfifo-by-triggered_buffer.patch 
```
Se tudo estiver certo você pode mandar o seu patch para os mantenedores utilizando o cliente de e-mail [neomutt](https://neomutt.org/).
```shell
neomutt -H <arquivo com o seu patch> 
```
Exemplo:
```shell
neomutt -H 0001-staging-iio-ad5933-replaced-kfifo-by-triggered_buffer.patch 
```

Pronto, o patch com suas alterações foi enviado para os mantenedores para que eles possam revisar o seu código e talvez incluí-lo ao restante do código do kernel linux.

[//]: comentário
<!-- comnetário de múltiplas linhas igual ao html
```shell
git log --pretty=oneline --abbrev-commit
```
comando para mandar patchset
```shell
git format-patch -o ~/patches --cover-letter -n --thread=shallow --to="lars@metafoo.de,Michael.Hennerich@analog.com,jic23@kernel.org,knaack.h@gmx.de,pmeerw@pmeerw.net,gregkh@linuxfoundation.org, michael.hennerich@analog.com,stefan.popa@analog.com,alexandru.Ardelean@analog.com" --cc="linux-iio@vger.kernel.org,devel@driverdev.osuosl.org,linux-kernel@vger.kernel.org,kernel-usp@googlegroups.com" 06caedbbe394^..348cbe2abbe
```

comentar que patch tem que ter o cabeçalho correto de acordo com o subsistema
ex: staging: iio: ad5933:

-->
[comment]: https://kernelnewbies.org/FirstKernelPatch

