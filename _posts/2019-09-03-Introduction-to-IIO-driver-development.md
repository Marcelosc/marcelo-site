---
layout: post
title:  "Introduction to IIO driver development"
date:   2019-09-03 11:20:11 -0300
categories: jekyll update
ref: introducing-to-iio
lang: en-us
---

Hi, my name is Marcelo, I'm a student at University of São Paulo, member of
[FLUSP](https://flusp.ime.usp.br/) and [HLUSP](http://hardwarelivreusp.org/)
students group, and Google Summer of Code (GSoC) student of 2019. Today, I
would like to share some experience I've acquired throughout the Linux
Foundation GSoC project on Analog Devices AD7292 device driver. The work done
during the first 3 months of project can be seen here: [Get the
code](https://github.com/marceloschmitt1/linux/tree/GSOC_2019_ad7292). I also
would like to thank my mentors: Dragos Bogdan, Stefan Popa, and Alexandru
Ardelean, who have been providing me guidance from long before the GSoC program
has started.

With this series of tutorials, I intend to provide a guide to introduce
newcomers to the Industrial I/O (IIO) subsystem. My approach will be pretty
much practical. Starting with pre-development steps such as cloning the
development repository, building the Linux kernel, installing helpful
development tools, to device tree settings and IIO specific functionality. To
illustrate the steps, I'll use AD7292 driver as an example. To the end of this
series of tutorials, a basic functional IIO driver will have been created from
scratch.

The following tutorials are listed such that it should make more sense to
follow them from the top to the bottom-most one. Hope they might be useful.

### [Raspberry Pi kernel compilation]({{ site.baseurl }}/2019/09/RaspberryPi-kernel-compilation)
This tutorial goes through most of the steps needed to build up Linux kernel
images for Raspberry Pi.

### Useful tools (coming soon)

### [Simple IIO driver]({{ site.baseurl }}/2019/09/Simple-IIO-driver)
This tutorial gives a brief introduction to the Industrial I/O (IIO) subsystem
and shows how to create a simple IIO device driver from scratch.

### [Device trees]({{ site.baseurl }}/2019/09/Device-trees) (coming soon)

### Device tree overlays (coming soon)

### IIO ADC channels (coming soon)

### Voltage regulator (coming soon)

### SPI read (coming soon)

If you find any typo or mistake in these tutorials please, send me an
e-mail (<marcelo.schmitt@usp.br>) and I'll be happy to update them with your
suggestions. Any feedback is welcome.

## A Brief Testimony about the GSoC Experience

The GSoC Analog Devices AD7292 device driver project was a great experience for
me. Throughout the 3 months of the project, I could learn fundamental
characteristics of IIO driver development under the guidance of extremely
skilled mentors who provided me valuable pieces of advice on how to proceed on
developing the AD7292 driver.

The mentors were very supportive and understanding. I have only good things to
speak of them. They were overall very responsive, providing me with the tools
and information every time I needed some aid to proceed with my tasks. They
were also sensitive, kindly giving me some time to focus on my college tasks
until I managed to dedicate the expected amount of time and effort on the
project. They were also very enthusiastic, encouraging me to keep up with a
FLOSS student group at my university. Finally, they were extremely polite,
always treating me with respect and manner. I am very thankful for having them
to help me with this project.

To me, it looks like this GSoC project was just the beginning of a great
partnership about learning and development into the Linux kernel IIO subsystem.
=)


<img src="{{ site.baseurl
}}/assets/images/2019-09-03-Introduction-to-IIO-driver-development/workspace.jpg"
width="800">

<span style="color:blue">
<em>
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
</em></span>

### Revision History:
- Rev1 (2019-09-03): Release
- Rev2 (2019-09-05): Added GSoC testimony

<!--

//TODOsssss
such as device initialization,
input/output channels, device trees, voltage regulators, SPI communication,
kernel compilation symbols and, a

STANDALONE MODE
The EVAL-AD7292SDZ can also be used without the EVAL-SDP-
CB1Z controller board. In this case, the customer application board
is connected to the digital interface using the J5 header connector
(or, if preferred, using the test points), an external V DRIVE voltage
(ranging from 1.8 V to 5.25 V) is connected to J2, and LK11 is
moved to Position B. Refer to the schematic diagram that is
available on the EVAL-AD7292SDZ Web page for more details.

Note: To convert a raw value to standard units, the IIO defines this formula: 
Scaled value = (raw + offset) * scale

https://wiki.st.com/stm32mpu/wiki/How_to_use_the_IIO_user_space_interface

What are each IIO_VAL define in include/linux/iio/types.h for?


config.txt

# Uncomment some or all of these to enable the optional hardware interfaces
#dtparam=i2c_arm=on
#dtparam=i2s=on
dtparam=spi=on

# Enable UART conection
dtoverlay=dwc2
enable_uart=1
# Device tree overlay for AD7768 ADC
#dtoverlay=rpi-ad7768
# Device tree debug flag
dtdebug=1
# Device tree overlay for AD7292 driver
dtoverlay=rpi-ad7292

cmdline.txt
dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=PARTUUID=ae888b82-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait

vim
ctags-universal
https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html

Also, some specific software may be required for the kernel to work properly
depending on which SoC it is going to run. The code for this lays on separated
directories. For instance, customized code to run on broadcom SoCs (Raspberry Pi
boards) is at *arch/arm/mach-bcm*. For OMAP SoCs (Beagle Bone boards), code is
at *arch/arm/mach-omap2*.

-->
